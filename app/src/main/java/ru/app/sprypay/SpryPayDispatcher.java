
package ru.app.sprypay;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
//http://stackoverflow.com/questions/35621605/android-using-shared-preference-and-dispatcher-activity-to-get-back-to-the-last
//http://stackoverflow.com/questions/2441203/how-to-make-an-android-app-return-to-the-last-open-activity-when-relaunched


public class SpryPayDispatcher extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Class<?> activityClass;

        try {
            SharedPreferences prefs = getSharedPreferences("X", MODE_PRIVATE);
            activityClass = Class.forName(prefs.getString("lastActivity", SpryPayLoginActivity.class.getName()));
        } catch(ClassNotFoundException ex) {
            activityClass = SpryPayLoginActivity.class;
        }

        startActivity(new Intent(this, activityClass));
    }
}