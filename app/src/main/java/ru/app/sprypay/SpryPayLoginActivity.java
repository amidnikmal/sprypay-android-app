package ru.app.sprypay;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;

import android.os.Build;

import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;

import android.view.inputmethod.EditorInfo;

import android.widget.Button;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.view.View.OnClickListener;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

//for pop up window
import android.view.ViewGroup.LayoutParams;

/**
 * A login screen that offers login via email/password.
 */
public class SpryPayLoginActivity extends AppCompatActivity {
    public static Intent intent = null; // Запускаем NavigationDrawerActivity
    public static final String SESSION_ID_PREFS = "SESSION_ID_PREFS";
    public static final String SPRY_PAY_LOGIN_REQUEST = "SPRY_PAY_LOGIN_REQUEST";
    RequestQueue queue;

    // UI references.
    private EditText mEmailView, mPasswordView;

    private View mProgressView, mLoginFormView;

    private LinearLayout mLoginLinearLayout;

    @Override /// TODO: Для чего используется @Override?
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login_activity);

        mLoginLinearLayout = (LinearLayout) findViewById(R.id.login_linear_layout);

        // Set up the login form.
        mEmailView = (EditText) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);

        /* Срабатывает когда после набора в поле пароля нажимаешь Enter */
        // https://developer.android.com/reference/android/widget/TextView.OnEditorActionListener.html
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });


        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    /*
    @Override
    public void onBackPressed() {
        Log.d("BACK IS PRESSED", "BACK IS PRESSED in SpryPatyLoginActivity");
        return;
    }*/

    private String md5(String in) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
            digest.reset();
            digest.update(in.getBytes());
            byte[] a = digest.digest();
            int len = a.length;
            StringBuilder sb = new StringBuilder(len << 1);
            for (int i = 0; i < len; i++) {
                sb.append(Character.forDigit((a[i] & 0xf0) >> 4, 16));
                sb.append(Character.forDigit(a[i] & 0x0f, 16));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) { e.printStackTrace(); }
        return null;
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            String url = "https://sprypay.ru/api/account.api.php?userEmail=" + email + "&userPasswd=" + md5(password);
            StringRequest stringRequest;

            queue = Volley.newRequestQueue(this);

            stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    PopupWindow popUpWindow = null;

                    try {
                        JSONObject resp = new JSONObject(response);
                        int responseStatus = resp.getInt("status");

                        // При запросе произошла ошибка - пользователь с такими данными не был найден
                        if (responseStatus == 0) {
                            LayoutInflater li = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                            View userHasntFoundPopUp = li.inflate(R.layout.user_not_found_popup_win,null);

                            TextView message = (TextView) userHasntFoundPopUp.findViewById(R.id.user_not_found);

                            // Устанавливаем полученное сообщение об ошибке в popUpWindow
                            message.setText(resp.getString("error"));

                            popUpWindow = new PopupWindow(
                                    userHasntFoundPopUp,
                                    LayoutParams.WRAP_CONTENT,
                                    LayoutParams.WRAP_CONTENT
                            );

                            // Set an elevation value for popup window
                            // Call requires API level 21
                            if(Build.VERSION.SDK_INT>=21) {
                                popUpWindow.setElevation(5.0f);
                            }

                            popUpWindow.showAtLocation(mLoginLinearLayout, Gravity.CENTER,0,0);

                            showProgress(false);
                        } else {
                            JSONObject data = resp.getJSONObject("data");

                            if (popUpWindow != null) {
                                popUpWindow.dismiss();
                                //popUpWindow = null;
                            }
                            String sessionId = data.getString("sessionId");

                            SharedPreferences sharedPref = getSharedPreferences(SESSION_ID_PREFS, 0);
                            SharedPreferences.Editor editor = sharedPref.edit();

                            editor.putString("SPRY_PAY_API_KEY", sessionId);
                            editor.commit();

                            // http://stackoverflow.com/questions/26830082/android-generated-loginactivity-errorincorrect-password
                            intent = new Intent(getBaseContext(), SpryPayDrawerActivity.class);
                            intent.putExtra("LOGIN_RESPONSE_DATA", response);

                            SpryPayLoginActivity.this.startActivity(intent);
                            finish();
                        }
                    } catch (JSONException e) {
                        Log.d("ERROR_JSONExcept", e.toString());
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Volley Exception", error.toString());
                }
            });

            stringRequest.setTag(SPRY_PAY_LOGIN_REQUEST);
            queue.add(stringRequest);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     *
     * @TargetApi - https://developer.android.com/reference/android/annotation/TargetApi.html
     *
     * Build.VERSION_CODES.HONEYCOMB_MR2 - https://developer.android.com/reference/android/os/Build.VERSION_CODES.html
     *
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences prefs = getSharedPreferences("X", MODE_PRIVATE);
        Editor editor = prefs.edit();
        editor.putString("lastActivity", getClass().getName());
        editor.commit();
    }
}