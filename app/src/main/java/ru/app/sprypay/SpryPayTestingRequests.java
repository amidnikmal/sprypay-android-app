package ru.app.sprypay;


import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class SpryPayTestingRequests  extends Activity {
    public static final String TAG = "MyTag";

    TextView mTextView;
    LinearLayout mLinearLayout;

    RequestQueue queue;
    StringRequest stringRequest;
    String url = "https://sprypay.ru/api/api.php?action=getPaymentSystemsTypes";
    String url2 = " https://sprypay.ru/api/account.api.php?userEmail=amidnikmal@yandex.ru&userPasswd=7a2a5f58b1bc3e6cafcbcbf28882ce23";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.testing_ssl);

        queue = Volley.newRequestQueue(this);

        mTextView = new TextView(this);
        mLinearLayout = (LinearLayout) findViewById(R.id.ssl_layout);

        stringRequest = new StringRequest(Request.Method.GET, url2, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mTextView.setText("Response is: " + response);
                mLinearLayout.addView(mTextView);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("TESTING VOLLEY", "That didn't work!");
            }
        });

        stringRequest.setTag(TAG);
        queue.add(stringRequest);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (queue != null) {
            queue.cancelAll(TAG);
        }
    }
}