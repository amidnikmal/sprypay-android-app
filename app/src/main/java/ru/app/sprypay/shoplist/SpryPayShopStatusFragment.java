package ru.app.sprypay.shoplist;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.app.sprypay.R;
import ru.app.sprypay.SpryPayEmptyFragment;

public class SpryPayShopStatusFragment extends Fragment {
    public SpryPayShopStatusFragment() {
    }

    public static SpryPayEmptyFragment newInstance(int someInt) {
        SpryPayEmptyFragment myFragment = new SpryPayEmptyFragment();
        return myFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.sprypay_shop_status_fragment, container, false);
        return rootView;
    }
}
