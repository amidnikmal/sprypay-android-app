package ru.app.sprypay.shoplist;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import android.content.SharedPreferences;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ru.app.sprypay.R;
;

public class SpryPayShopListFragment extends Fragment {
    // Для получение свойств приложения
    public static final String PREFS_NAME = "MyPrefsFile"; // TODO: поменять на что нибудь понятное

    // Запрос Volley
    public static final String TAG = "getSpryPayShopList";
    RequestQueue queue;

    /* Заголовки магазинов */
    List<JSONObject> titles;
    SpryPayShopListAdapter spryPayShopListAdapter;
    ListView lv;

    public String spryPayApiKey = null;

    public SpryPayShopListFragment() { /* do something */ }

    public static SpryPayShopListFragment newInstance(int someInt) {
        SpryPayShopListFragment myFragment = new SpryPayShopListFragment();
        return myFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.sprypay_shop_list, container, false);
        lv = (ListView) rootView.findViewById(R.id.shop_list);
        titles = new ArrayList<JSONObject>();

        // Get SpryPay api key from global prefs store
        Context context = getActivity();
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        spryPayApiKey = settings.getString("SPRY_PAY_API_KEY", null);

        // Make request to get shops list of current user
        String url = "https://sprypay.ru/api/account.api.php?" +
                "sessionId=" + spryPayApiKey +
                "&apiAction=getUserShops";

        StringRequest stringRequest;

        queue = Volley.newRequestQueue(context);
        stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("shop list", response);

                try {
                    JSONObject data = new JSONObject(response).getJSONObject("data");
                    JSONArray shopList = new JSONArray(data.getString("userShopsList"));

                    Log.d("shopList", shopList.toString());

                    for (int i=0; i<shopList.length(); i++) {
                        titles.add(shopList.getJSONObject(i));
                    }

                    spryPayShopListAdapter = new SpryPayShopListAdapter(getActivity(), titles);

                    lv.setAdapter(spryPayShopListAdapter);

                    lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                            //Here you can get the position and access your
                            //TicketList Object

                            Log.d("setOnItemClickListener", "setOnItemClickListener fire");

                            TabbetFragment tabbetFragment = new TabbetFragment();

                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.beginTransaction()
                                    .replace(R.id.flContent, tabbetFragment)
                                    .addToBackStack("shop")
                                    .commit();
                        }
                    });

                    //String sessionId = data.getString("sessionId");
                } catch (JSONException e) {
                    Log.d("ERROR_JSONExcept", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("TEEEST", error.toString());
                Log.d("TESTING VOLLEY", "That didn't work!");
            }
        });

        stringRequest.setTag(TAG);
        queue.add(stringRequest);



        return rootView;
    }
}