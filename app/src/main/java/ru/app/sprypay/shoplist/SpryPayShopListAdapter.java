package ru.app.sprypay.shoplist;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import ru.app.sprypay.R;

public class SpryPayShopListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<JSONObject> shopsList;

    public SpryPayShopListAdapter(Activity activity, List<JSONObject> titles) {
        this.activity = activity;
        this.shopsList = titles;
    }

    @Override
    public int getCount() {
        return shopsList.size();
    }

    @Override
    public Object getItem(int location) {
        return shopsList.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.sprypay_shop_list_item, null);

        TextView shopTitle = (TextView) convertView.findViewById(R.id.shop_title);
        TextView shopId = (TextView) convertView.findViewById(R.id.shop_id);
        TextView shopStatus = (TextView) convertView.findViewById(R.id.shop_status);
        TextView shopUrl = (TextView) convertView.findViewById(R.id.shop_url);

        //shop_number

        try {
            shopTitle.setText(shopsList.get(position).getString("shopName"));
            shopId.setText(shopsList.get(position).getString("shopId"));
            shopUrl.setText(shopsList.get(position).getString("shopUrl"));

            if (Integer.parseInt(shopsList.get(position).getString("adminSiteCheck")) == 1) {
                shopStatus.setText("Проверено администратором");
                shopStatus.setTextColor(Color.BLUE);
            } else if (Integer.parseInt(shopsList.get(position).getString("adminSiteCheck")) == 0) {
                shopStatus.setText("Не проверено администратором");
                shopStatus.setTextColor(Color.RED);
            }

        } catch (JSONException e) {
            Log.d("ERROR_JSONExcept", e.toString());
        }
/*
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("HEEEELLLLLOOOOOO2", "HEEEELLLLLOOOOOO2");
            }

        });
*/
        return convertView;
    }
}