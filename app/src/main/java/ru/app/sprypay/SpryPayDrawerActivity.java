package ru.app.sprypay;

import ru.app.sprypay.news.SpryPayNewsFragment;
import ru.app.sprypay.paymentlist.SpryPayPaymentListFragment;
import ru.app.sprypay.profile.SpryPayProfileOnlyLinearLayoutFragment;
import ru.app.sprypay.shoplist.*;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class SpryPayDrawerActivity extends AppCompatActivity {

    private Menu mOptionsMenu;
    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    private NavigationView nvDrawer;

    private SpryPayProfileOnlyLinearLayoutFragment spryPayProfileOnlyLinearLayoutFragment = new SpryPayProfileOnlyLinearLayoutFragment();
    private SpryPayDialogFragment spryPayDialogFragment = new SpryPayDialogFragment();
    private SpryPayEmptyFragment secondFragment = new SpryPayEmptyFragment();
    private SpryPayPaymentListFragment spryPayPaymentsListFragment = new SpryPayPaymentListFragment();
    private SpryPayNewsFragment spryPayNewsFragment = new SpryPayNewsFragment();
    private SpryPayShopListFragment spryPayShopListFragment = new SpryPayShopListFragment();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        mOptionsMenu = menu;
        return true;
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = this.getSupportFragmentManager();
        fm.popBackStack();
        return;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set a Toolbar to replace the ActionBar.
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Find our drawer view
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        nvDrawer = (NavigationView) findViewById(R.id.nvView); // activity_main.xml
        // Setup drawer view

        TextView headerUserEmail = (TextView) nvDrawer.getHeaderView(0).findViewById(R.id.navHeaderUserEmail);
        TextView headerUserBalance = (TextView) nvDrawer.getHeaderView(0).findViewById(R.id.navHeaderUserBalance);

        if (headerUserEmail == null) {
            Log.d("ERROR_DrawerActivity", "headerUserEmail");
        } else {
            try {
                JSONObject data = new JSONObject(getIntent().getStringExtra("LOGIN_RESPONSE_DATA")).getJSONObject("data");

                JSONObject userData = data.getJSONObject("userData");
                String userEmail = userData.getString("userEmail");
                String userBalance = userData.getString("userBalance");
                String userId = userData.getString("userId");

                headerUserEmail.setText(userEmail + " (" + userId + ")");
                headerUserBalance.setText("Баланс: " + userBalance);
            } catch (JSONException e) {
                Log.d("JSONException", e.toString());
            }
        }


        getSupportFragmentManager()
                .beginTransaction()
                    .add(R.id.flContent, spryPayNewsFragment)
                    .commit();

        setupDrawerContent(nvDrawer);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the fragment to show based on nav item clicked
        FragmentManager fragmentManager = getSupportFragmentManager();

        switch(menuItem.getItemId()) {
            case R.id.nav_news:
                mOptionsMenu.clear();
                fragmentManager.beginTransaction()
                        .replace(R.id.flContent, spryPayNewsFragment)
                        .addToBackStack("news")
                        .commit();
                break;

            case R.id.nav_profile:
                mOptionsMenu.clear();
                fragmentManager.beginTransaction()
                        .replace(R.id.flContent, spryPayProfileOnlyLinearLayoutFragment)
                        .addToBackStack("profile")
                        .commit();
                break;

            case R.id.nav_payments_list:
                mOptionsMenu.clear();
                fragmentManager.beginTransaction()
                        .replace(R.id.flContent, spryPayPaymentsListFragment)
                        .addToBackStack("payments")
                        .commit();

                break;

            case R.id.nav_shop_list:
                mOptionsMenu.clear();
                fragmentManager.beginTransaction()
                        .replace(R.id.flContent, spryPayShopListFragment)
                        .addToBackStack("shop_list")
                        .commit();
                break;

            default:
                mOptionsMenu.clear();
                fragmentManager.beginTransaction()
                        .replace(R.id.flContent, secondFragment)
                        .addToBackStack("second_fragment")
                        .commit();
                break;
        }

        // Highlight the selected item has been done by NavigationViewZZ
        menuItem.setChecked(true);

        // Set action bar title
        setTitle(menuItem.getTitle());

        // Close the navigation drawer
        mDrawer.closeDrawers();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}