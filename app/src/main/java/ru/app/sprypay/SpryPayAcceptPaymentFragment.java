package ru.app.sprypay;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class SpryPayAcceptPaymentFragment extends Fragment {
    public SpryPayAcceptPaymentFragment() {
    }

    public static SpryPayAcceptPaymentFragment newInstance(int someInt) {
        SpryPayAcceptPaymentFragment myFragment = new SpryPayAcceptPaymentFragment();
        return myFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.sprypay_accept_payment, container, false);
        return rootView;
    }
}
