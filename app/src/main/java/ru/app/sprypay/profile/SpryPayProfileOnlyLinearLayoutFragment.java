package ru.app.sprypay.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.app.sprypay.R;


public class SpryPayProfileOnlyLinearLayoutFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sprypay_profile_only_linearlayout_fragment,null);
    }

    public void my_on_click(View view) {
        SpryPayDialogFragmentTest spryPayDialogFragmentTest = new SpryPayDialogFragmentTest();
        spryPayDialogFragmentTest.show(getFragmentManager(), "myTag");
    }

    public void my_on_click2(View view) {
        SpryPayIPaddrDialogFragment spryPayIPaddrDialogFragment = new SpryPayIPaddrDialogFragment();
        spryPayIPaddrDialogFragment.show(getFragmentManager(), "myTag2");
    }

    public void my_on_click3(View view) {
        SpryPaySecretWordDialogFragment spryPaySecretWordDialog = new SpryPaySecretWordDialogFragment();
        spryPaySecretWordDialog.show(getFragmentManager(), "myTag3");
    }


    public void my_on_click4(View view) {
        SpryPayPasswordDialogFragment spryPayPasswordDialogFragment = new SpryPayPasswordDialogFragment();
        spryPayPasswordDialogFragment.show(getFragmentManager(), "myTag4");
    }
}