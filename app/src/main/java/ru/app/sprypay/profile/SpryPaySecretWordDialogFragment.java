package ru.app.sprypay.profile;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.EditText;

import ru.app.sprypay.R;

public class SpryPaySecretWordDialogFragment extends DialogFragment implements View.OnClickListener {

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle("Title!");
        View v = inflater.inflate(R.layout.sprypay_secret_word_dialog, null);

        v.findViewById(R.id.btnYes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mySaveOnClickListener(v);
            }
        });

        v.findViewById(R.id.btnNo).setOnClickListener(this);

        EditText oldEditText = (EditText) v.findViewById(R.id.old_secret_word);
        EditText newEditText = (EditText) v.findViewById(R.id.new_secret_word);

        //oldEditText.setOnFocusListener(new

        //v.findViewById(R.id.btnMaybe).setOnClickListener(this);
        return v;
    }

    public void mySaveOnClickListener(View v) {
        SpryPayProfileOnlyLinearLayoutFragment getProfileFragment = (SpryPayProfileOnlyLinearLayoutFragment) getFragmentManager().findFragmentByTag("spryPayProfileOnlyLinearLayoutFragment_TAG");
        View profileView = getProfileFragment.getView();

        SpryPaySecretWordDialogFragment getDialogFragment = (SpryPaySecretWordDialogFragment) getFragmentManager().findFragmentByTag("myTag3");
        View dialogView = getDialogFragment.getView();

        EditText oldEditText = (EditText) dialogView.findViewById(R.id.old_secret_word);
        EditText newEditText = (EditText) dialogView.findViewById(R.id.new_secret_word);

        dismiss();
    }

    public void onClick(View v) {
        //Log.d(LOG_TAG, "Dialog 1: " + ((Button) v).getText());
        dismiss();
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        //Log.d(LOG_TAG, "Dialog 1: onDismiss");
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        //Log.d(LOG_TAG, "Dialog 1: onCancel");
    }

}