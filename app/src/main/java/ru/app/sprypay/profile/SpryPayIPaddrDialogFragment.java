
package ru.app.sprypay.profile;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import ru.app.sprypay.R;

public class SpryPayIPaddrDialogFragment extends DialogFragment implements View.OnClickListener {
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle("Title!");
        View v = inflater.inflate(R.layout.sprypay_ip_dialog, null);



        v.findViewById(R.id.btnYes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mySaveOnClickListener(v);
            }
        });

        v.findViewById(R.id.btnNo).setOnClickListener(this);
        //v.findViewById(R.id.btnMaybe).setOnClickListener(this);
        return v;
    }

    public void mySaveOnClickListener(View v) {
        SpryPayProfileOnlyLinearLayoutFragment getProfileFragment = (SpryPayProfileOnlyLinearLayoutFragment) getFragmentManager().findFragmentByTag("spryPayProfileOnlyLinearLayoutFragment_TAG");
        View profileView = getProfileFragment.getView();

        SpryPayIPaddrDialogFragment getDialogFragment = (SpryPayIPaddrDialogFragment) getFragmentManager().findFragmentByTag("myTag2");
        View dialogView = getDialogFragment.getView();

        TextView t = (TextView) profileView.findViewById(R.id.ip_addr);

        EditText e = (EditText) dialogView.findViewById(R.id.ip_edit_text);



        if (e != null) {
            t.setText(e.getText().toString());
        } else {
            t.setText("Error");
        }

        dismiss();
    }

    public void onClick(View v) {
        //Log.d(LOG_TAG, "Dialog 1: " + ((Button) v).getText());
        dismiss();
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        //Log.d(LOG_TAG, "Dialog 1: onDismiss");
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        //Log.d(LOG_TAG, "Dialog 1: onCancel");
    }
}