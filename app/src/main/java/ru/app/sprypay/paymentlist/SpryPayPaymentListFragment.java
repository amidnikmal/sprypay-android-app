package ru.app.sprypay.paymentlist;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ru.app.sprypay.R;
import ru.app.sprypay.shoplist.SpryPayShopListAdapter;
import ru.app.sprypay.shoplist.TabbetFragment;

public class SpryPayPaymentListFragment extends Fragment {
    View rootView;
    ExpandableListView lv;
    private String[] groups; // Группы - это заголовки
    private String[][] children; // Контент элемента группы

    // Для получение свойств приложения
    public static final String PREFS_NAME = "MyPrefsFile"; // TODO: поменять на что нибудь понятное

    // Запрос Volley
    public static final String TAG = "getSpryPayPaymentsList";
    RequestQueue queue;

    public String spryPayApiKey = null;

    public SpryPayPaymentListFragment() {
    }

    public static SpryPayPaymentListFragment newInstance(int someInt) {
        return new SpryPayPaymentListFragment();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
/*
        // Get SpryPay api key from global prefs store
        Context context = getActivity();
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        spryPayApiKey = settings.getString("SPRY_PAY_API_KEY", null);

        // Make request to get shops list of current user
        String url = "https://sprypay.ru/api/account.api.php?" +
                "sessionId=" + spryPayApiKey +
                "&apiAction=getUserPayments";

        StringRequest stringRequest;

        queue = Volley.newRequestQueue(context);
        stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("payments list", response);


                try {
                    JSONObject data = new JSONObject(response).getJSONObject("data");
                    JSONArray shopList = new JSONArray(data.getString("userShopsList"));

                    Log.d("shopList", shopList.toString());

                    for (int i=0; i<shopList.length(); i++) {
                        groups[i] = shopList.getJSONObject(i);
                    }

                } catch (JSONException e) {
                    Log.d("ERROR_JSONExcept", e.toString());
                }


            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("TEEEST", error.toString());
                Log.d("TESTING VOLLEY", "That didn't work!");
            }
        });

        stringRequest.setTag(TAG);
        queue.add(stringRequest);
*/

        groups = new String[]{
                "2018345345",
                "2018234545",
                "2018223433",
                "2018787777"
            };


        children = new String[][]{
                {"test 1"},

                {"test 2"},

                {"test 3"},

                {"Test 4"}
        };
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_lineup, container, false);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lv = (ExpandableListView) view.findViewById(R.id.expListView);
        lv.setAdapter(new ExpandableListAdapter(groups, children, getActivity()));
        lv.setGroupIndicator(null);
    }

}