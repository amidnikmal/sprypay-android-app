package ru.app.sprypay;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class SpryPayEmptyFragment extends Fragment {
    public SpryPayEmptyFragment() {
    }

    public static SpryPayEmptyFragment newInstance(int someInt) {
        SpryPayEmptyFragment myFragment = new SpryPayEmptyFragment();
        return myFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.payments_list_fragment, container, false);
        return rootView;
    }
}
