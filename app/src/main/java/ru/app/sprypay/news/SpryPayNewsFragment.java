package ru.app.sprypay.news;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import ru.app.sprypay.R;
import ru.app.sprypay.SpryPayEmptyFragment;


public class SpryPayNewsFragment extends Fragment {
    public SpryPayNewsFragment() {
    }

    public static SpryPayEmptyFragment newInstance(int someInt) {
        SpryPayEmptyFragment myFragment = new SpryPayEmptyFragment();
        return myFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.news_list, container, false);

        ListView listView = (ListView) rootView.findViewById(R.id.news_list);

        List<FeedItem> feedItems = new ArrayList<FeedItem>();;

        FeedItem feedItem = new FeedItem();

        feedItems.add(feedItem);
        feedItems.add(feedItem);
        feedItems.add(feedItem);
        feedItems.add(feedItem);
        feedItems.add(feedItem);
        feedItems.add(feedItem);
        feedItems.add(feedItem);
        feedItems.add(feedItem);
        feedItems.add(feedItem);
        feedItems.add(feedItem);

        SpryPayNewsListAdapter spryPayNewsListAdapter = new SpryPayNewsListAdapter(getActivity(), feedItems);
        listView.setAdapter(spryPayNewsListAdapter);

        /*
        listView.setOnClickListener(new View.OnClickListener() {
            @Override
                public void onItemClick(AdapterView arg0, View view, int position, long id) {
                    // user clicked a list item, make it "selected"
                    selectedAdapter.setSelectedPosition(position);
            }
        });
*/
        /*
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                //Here you can get the position and access your
                //TicketList Object

                Log.d("HEEEELLLLLOOOOOO", "HEEEELLLLLOOOOOO");
            }
        });
        */

        return rootView;
    }

}
